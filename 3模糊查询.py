#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '模型类实现.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
    print('-'*30)
    from app.models import *

    # 查询用户名中包含'1'的用户
    user1_s = User.objects.filter(username__contains='1')
    print(user1_s)
    # 查询用户名已以'name1'开头的用户
    user2_s = User.objects.filter(username__startswith='name1')
    print(user2_s)
    # 查询邮箱以'@qq.com'结尾的用户
    user3_s = User.objects.filter(email__endswith='@qq.com')
    print(user2_s)






