#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '模型类实现.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
    print('-' * 30)
    from app.models import *

    user2 = User.objects.get(id=2)
    print(user2.username)
    user1 = User.objects.get(id=1)
    print(type(user1))
    print(user1)
    try:
        User.objects.get(id=10000)  # 没有id=10000的对象
    except:
        print('没有满足条件的对象，抛出异常')
    try:
        User.objects.get(isdelete=0)  # 多个对象满足条件
    except:
        print('多个对象满足条件，抛出异常')
