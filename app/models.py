import datetime

from django.db import models

# Create your models here.
class User(models.Model):
    account = models.CharField(max_length=16, unique=True)  # 登录账号
    username = models.CharField(max_length=16, null=True, db_index=True)  # 用户昵称
    password = models.CharField(max_length=32)  # 密码
    gender = models.IntegerField(default=0)  # 性别，0：男，1：女
    birthday = models.DateField(null=True)  # 生日
    money = models.DecimalField(max_digits=10, decimal_places=2, default=0)  # 余额，总位数10，小数位2，默认0
    email = models.EmailField(null=True)  # 邮箱
    image = models.ImageField(null=True)  # 头像
    createDatetime = models.DateTimeField(auto_now_add=datetime.datetime.now())  # 创建时间
    updateDatetime = models.DateTimeField(auto_now=datetime.datetime.now())  # 最近一次修改时间
    isdelete = models.BooleanField(default=False)  # 逻辑删除

    # def __str__(self):

# 创建user表数据
def build_user(num=100):
        for i in range(num):
            User.objects.create(
                account='%05d' % i,
                username='name%s' % i,
                password=str(i),
                gender=i % 2,
                birthday=datetime.datetime.now() + datetime.timedelta(days=i),
                money=i ** 2 + 12.34 * i + 100,
                email='%05d@qq.com' % i,
                createDatetime=datetime.datetime.now(),
                updateDatetime=datetime.datetime.now(),
                isdelete=i % 2,
            )

class VIP(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=16)
    gender = models.PositiveSmallIntegerField()
    age = models.IntegerField()
    createtime = models.DateTimeField()
