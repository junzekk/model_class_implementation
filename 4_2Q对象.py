#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '模型类实现.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
    print('-'*30)
    from app.models import *

    from django.db.models import Q

    # 查询余额大于5000块的男性
    print(User.objects.filter(money__gt=5000, gender=0))  # 第一种 逗号
    print(User.objects.filter(Q(money__gt=5000) & Q(gender=0)))  # 第二种 Q(【条件】)&Q(【条件】)
    # 查询余额大于5000块的男性和余额大于2000的女性
    print(User.objects.filter(Q(money__gt=5000, gender=0) | Q(money__gt=2000, gender=1)))








