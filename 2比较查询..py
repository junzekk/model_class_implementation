#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '模型类实现.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
    print('-'*30)
    from app.models import *

    # 没有被逻辑删除的User
    user1_s = User.objects.filter(isdelete=0)
    print(user1_s)
    user2_s = User.objects.filter(isdelete__exact=0)
    print(user2_s)
    # 查询余额大于等于1000的User对象
    user3_s = User.objects.filter(money__gte=1000)
    print(user3_s)
    # 查询生日为空的User对象
    user4_s = User.objects.filter(birthday__isnull=True)
    print(user4_s)








